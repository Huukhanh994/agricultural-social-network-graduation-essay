$(document).ready(function () {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var relationship_id = $("input[name=relationship_id]").val();
    var status_accept = $("input[name=status_accept]").val();
    var status_deny = $("input[name=status_deny]").val();
    var sender_id = $("input[name=sender_id]").val();
    var receiver_id = $("input[name=receiver_id]").val();
    var receiver_id_request = $("input[name=receiver_id_request]").val();
    $('.acceptRequestFriend').click(function (e) {
        e.preventDefault();
        var class_key = $(this).parent().parent().attr("class").substr(0, 1);
        console.log(class_key);
        $.ajax({
            type: 'POST',
            url: "/relationship/ajaxSendRequestFriend",
            data: { receiver_id_request: receiver_id_request},
            success: function (data) {
                if (data.success_add_friend) {
                    $('.' + class_key + 'inline-items').hide();
                    alert('Send request friend successfully!');
                }
            }
        });
    })

    $(".acceptFriend").click(function (e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: "/relationship/ajaxFriend",
            data: { status_accept: status_accept,relationship_id:relationship_id},
            success: function (data) {
                $('.acceptFriend').attr('hidden', 'hidden');
                if (data.success_add) {
                    alert('Add friend successfully!');
                }
            }
        });
    });

    $('.denyFriend').click(function (e) {
        e.preventDefault();
        $.ajax({
            type: 'POST',
            url: "/relationship/ajaxFriend",
            data: { status_deny: status_deny, relationship_id: relationship_id },
            success: function (data) {
                $('.denyFriend').attr('hidden', 'hidden');
                if (data.success_deny) {
                    alert('Cancel friend successfully!');
                }
            }
        });
    });



});
